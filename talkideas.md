# agenda
- me
- you

---
# me

# Presenter Notes
just a guy

I did some training in school in CS
I got trial by fire on the business side and the people side; I'm encouraging you to not "stop" at the edge of IT

who loves to get things to work, and solve real problems with them

I've worked very hard to understand technology, business, and people

I'm a generalist - is this bad?

---
# IT
![IT](IT-The-Great-Monolith.jpg)

# Presenter Notes
what is IT?

what isn't it? building software? setting up email?

who's heard of the cloud?

everything is moving to software, including cars, smartphones, etc.

More and more businesses will need to do actual sofware engineering, regardless of whether it's their product or not

Salesforce.com implementation at iContact, spent more $ on custom software work, some internal, some not, than we did on the licenses

you will _need_ to learn more about technology to succeed

---
# shoes

# Presenter Notes
how do you walk a mile in people's shoes?

from the outside, does a job look easy or hard?

how hard is it to be a janitor? shadow, motivation? pride, quality of work

Don't say something is easy unless you've already done it, how would I think differently if I were the boss?  How can I make his/her job easier?

---
# what technologies?

# Presenter Notes
Oracle vs. Open Source

Just to get on the record, it's a FAIR question.

---
# people

# Presenter Notes
hire slowly fire quickly

all about the people

A team, B team, and C team players

one person, wrote lots of software on their own, but didn't write things peopel could maintain, this didn't help, makes everything take longer over time

---
# how long will this take?

# Presenter Notes
This is the favorite question from people to IT.

email: gmail vs. aol

Just to get on the record, it's a FAIR question.

However, there's this large uncertainty problem

Agile?

---
# uncertainty
![cone](Cone02.jpg)

# Presenter Notes
how do I mitigate this?

demo early and often to demystify

---
# business strategy

# Presenter Notes
what if I'm trying to design a system, but every week we get confused about who we're selling a product to?

---
# data

# Presenter Notes
understand your data

what are its limitations?

verify carefuly

think like a scientist

examples of disconnects between people

this is also what you look for in technologists!

how many customers do you have?

---
# why

# Presenter Notes
I've succeeded not because of what I know about technology, but more importantly 
by never resiting the urge to delve beyond technology to work in other realms. 
That doesn't mean I'm an expert, but I know more about double-entry accounting 
than I ever thought I would, because it affects how I build good technology to 
support a business.

---
# simlicity

# Presenter Notes
simple tools aren't bad

---
# credits
- http://construx.com/Page.aspx?cid=1648
